package com.generators.schema.data.integration;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.generators.schema.data.configuration.ConversionConfiguration;
import com.generators.schema.data.configuration.MessagesConfiguration;
import com.generators.schema.data.configuration.RequirementsConfiguration;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureMockMvc
public class IntegrationTests {

    @Autowired
    MockMvc mvc;
    
    @Autowired
    ConversionConfiguration config;
    
    @Autowired
    MessagesConfiguration messages;
    
    @Autowired
    RequirementsConfiguration requirements;

    private MockMultipartFile getMultipartFromFile(String filename) throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());
        FileInputStream input = new FileInputStream(file);
       return new MockMultipartFile("file",
                file.getName(), "text/plain", IOUtils.toByteArray(input));
    }

    @ParameterizedTest
    @ValueSource(strings = {"fruitAndVegSchema.json", "timestamp.json"})
    public void jsonSchemas(String name) throws Exception {
        MvcResult response = mvc.perform(
        		MockMvcRequestBuilders.multipart("/generate/schema")
                .file(getMultipartFromFile("test-schemas/" + name))
        ).andReturn();

        assertEquals(202,response.getResponse().getStatus());
    }

}
