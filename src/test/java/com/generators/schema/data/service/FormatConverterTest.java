package com.generators.schema.data.service;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;

import javax.xml.transform.TransformerException;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class FormatConverterTest {

    @Autowired
    FormatConverter converter;

    @Autowired
    Linter linter;

    String srcDir = "src/test/resources/test-schemas/";
    
    String name = "JsonToXSDSchema";

    @ParameterizedTest
    @ValueSource(strings = {"fruitAndVegSchema.json"})
    void testJsonToXSD(String filename) throws IOException, TransformerException {
        jsonToXsdFunctionality(filename);
    }

    //TODO Investigate issues with this schema
    //java.lang.IllegalArgumentException: "properties" property should be found in root of JSON schema"
    @Disabled
    @ParameterizedTest
    @ValueSource(strings = {"peopleVehiclesSchema.json"})
    void testOneOfJsonToXSD(String filename) throws IOException, TransformerException {
        jsonToXsdFunctionality(filename);
    }

    private void jsonToXsdFunctionality(String filename) throws IOException, TransformerException {
        String content = FileUtils.readFileToString(new File(srcDir.concat(filename)));
        String xsd = converter.convertJsonToXsd(content, name);
        assertTrue(linter.isValidXml(xsd));
    }
}
