package com.generators.schema.data.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.generators.schema.data.model.TestClass;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class LinterTest {
	
	@Autowired
	Linter linter;

	@Test
	void testIllegalField() throws NoSuchFieldException, SecurityException {
		Field field = TestClass.class.getDeclaredField("stringField");
		assertThrows(IllegalArgumentException.class, ()-> linter.processField(field));
	}
	
	@Test
	void testOptionalField() throws NoSuchFieldException, SecurityException {
		Field field = TestClass.class.getDeclaredField("falseJsonStringField");
		assertNull(linter.processField(field));
	}
	
	@ParameterizedTest
	@CsvSource({
		"jsonStringField, String",
		"jsonList, List",
		"jsonInt, int"
	})
	void testValidFields(String name, String type) throws NoSuchFieldException, SecurityException {
		Field field = TestClass.class.getDeclaredField(name);
		assertEquals(type, linter.processField(field));
	}

	@Test
	void testValidJson() throws IOException {
		String body = FileUtils.readFileToString(new File("src/test/resources/test-schemas/fruitAndVegSchema.json"));
		assertTrue(linter.isValidJson(body));
	}

}
