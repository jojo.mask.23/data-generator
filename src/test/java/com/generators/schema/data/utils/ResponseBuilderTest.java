package com.generators.schema.data.utils;


import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.JsonProcessingException;

public class ResponseBuilderTest {

    @ParameterizedTest
    @CsvSource({
            "SERVER_ERROR, Error message, 500"
    })
    void testMessageGenerated(String code, String reason, String statusCode) throws JsonProcessingException {
        HttpStatus status = HttpStatus.valueOf(Integer.parseInt(statusCode));
        ResponseEntity<String> response = ResponseBuilder.constructResponse(code, reason, status);
        assertTrue(response.getBody().contains(code));
        assertTrue(response.getBody().contains(reason));
        assertTrue(response.getStatusCode().equals(status));

    }
    
    @Test
    void testDefaultResponse() throws JsonProcessingException {
    	ResponseEntity<String> response = ResponseBuilder.defaultErrorResponse();
    	assertTrue(response.getBody().contains("SERVER_ERROR"));
    	assertTrue(response.getBody().contains("Unexpected Error Occurred During Runtime"));
    	assertTrue(response.getStatusCode().equals(HttpStatus.valueOf(500)));
    	
    }

}
