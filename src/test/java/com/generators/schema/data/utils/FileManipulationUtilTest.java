package com.generators.schema.data.utils;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

public class FileManipulationUtilTest {

    String resourcesDir = "src/test/resources/test-schemas/";


    @ParameterizedTest
    @ValueSource(strings = {"fruitAndVegSchema.json"})
    void testConvert(String filename) throws IOException {
        File file = new File(resourcesDir.concat(filename));
        FileInputStream input = new FileInputStream(file);
        MultipartFile multipartFile = new MockMultipartFile("file",
                file.getName(), "text/plain", IOUtils.toByteArray(input));
        assertNotNull(FileManipulationUtil.convert(multipartFile));
    }
}
