package com.generators.schema.data.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TestClass {
    @SuppressWarnings("unused")
	private String stringField;
    
    @JsonProperty(required=true)
	private List<String> jsonList;
    
    @JsonProperty(required=true)
    private int jsonInt;

    @JsonProperty(required=true)
    private String jsonStringField;

    @JsonProperty(required=false)
    private String falseJsonStringField;
    
    
}
