package com.generators.schema.data.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

@Jacksonized
@Builder
@Data
@AllArgsConstructor
public class FrontendErrorResponse {

    @JsonProperty("code")
    private String code;
    @JsonProperty("reason")
    private String reason;

}