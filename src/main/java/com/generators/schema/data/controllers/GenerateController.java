package com.generators.schema.data.controllers;

import java.io.IOException;

import javax.xml.transform.TransformerException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.generators.schema.data.configuration.ConversionConfiguration;
import com.generators.schema.data.configuration.MessagesConfiguration;
import com.generators.schema.data.service.DataGenerator;
import com.generators.schema.data.service.FormatConverter;
import com.generators.schema.data.service.Linter;
import com.generators.schema.data.utils.FileManipulationUtil;
import com.generators.schema.data.utils.ResponseBuilder;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("generate")
@AllArgsConstructor
@Slf4j
public class GenerateController {

    MessagesConfiguration messages;

    DataGenerator generator;

    Linter linter;

    FormatConverter converter;
    @Autowired
    ConversionConfiguration convertConfig;

    @PostMapping("schema")
    public ResponseEntity<String> generateFromSchema(@RequestParam(name = "file", required = true) MultipartFile file)
            throws JsonProcessingException {
        try {
            String converted = FileManipulationUtil.convert(file);
            if (linter.isValidJson(converted)) {
                converted = converter.convertJsonToXsd(converted, convertConfig.getElementName());
                log.info("XSD Received: " + converted);
                String payload = generator.generateXmlFromDocument(converted, convertConfig.getElementName());
                return ResponseBuilder.constructSuccessMessage(converter.convertXMLToJson(payload));
            } else if (linter.isValidYaml(converted)) {
                converted = converter.convertYamlToJson(converted);
                converted = converter.convertJsonToXsd(converted, convertConfig.getElementName());
                // TODO Implement method to generate data from XSD
                return ResponseBuilder.constructResponse(messages.getNotImplementedCode(),
                        "YAML Method not constructed yet", HttpStatus.NOT_IMPLEMENTED);
            } else if (linter.isValidXml(converted)) {
                String generated = generator.generateXmlFromDocument(converted, convertConfig.getElementName());
               return ResponseBuilder
            		   .constructSuccessMessage
            		   (converter.convertXMLToJson(generated));
            } else {
                log.error("Payload File supplied is Neither XML, Json or Yaml");
                return ResponseBuilder.constructResponse(messages.getBadRequestCode(),
                        "Schema Supplied is neither JSON, YAML or XSD", HttpStatus.BAD_REQUEST);
            }
        } catch (IOException | TransformerException e) {
            log.error("Exception Thrown: " + e.getMessage());
            return ResponseBuilder.defaultErrorResponse();
        }
    }

    // TODO Set Response Type and also ensure we can get a flat JSON Response from a file.
    @PostMapping("jsonSchema")
    public ResponseEntity<String> generateFromJsonSchema(
            @RequestParam(name = "file", required = true) MultipartFile file) throws JsonProcessingException {
        try {

            String convert = FileManipulationUtil.convert(file);
            if (!linter.isValidJson(convert)) {
                return ResponseBuilder.constructResponse("BAD_REQUEST", "Schema Supplied is not valid JSON",
                        HttpStatus.BAD_REQUEST);
            }
            convert = converter.convertJsonToXsd(convert, convertConfig.getElementName());
            log.info("XSD Received: " + convert);
            return ResponseBuilder.constructResponse("NOT_IMPLEMENTED", "XML to JSON not constructed yet",
                    HttpStatus.NOT_IMPLEMENTED);
        } catch (IOException | TransformerException e) {
            log.error("Exception Thrown: " + e.getMessage());
            return ResponseBuilder.defaultErrorResponse();
        }

    }
}
