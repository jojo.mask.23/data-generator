package com.generators.schema.data.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.generators.schema.data.model.FrontendErrorResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ResponseBuilder {

    private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public static ResponseEntity<String> constructResponse(String code, String reason, HttpStatus status)
            throws JsonProcessingException {

        return new ResponseEntity<String>(gson.toJson(new FrontendErrorResponse(code, reason)), status);
    }

    public static ResponseEntity<String> defaultErrorResponse() {
        return new ResponseEntity<String>(
                gson.toJson(new FrontendErrorResponse("SERVER_ERROR", "Unexpected Error Occurred During Runtime")),
                HttpStatus.valueOf(500));

    }

    public static ResponseEntity<String> constructSuccessMessage(String payload) {
        return new ResponseEntity<String>(payload, HttpStatus.ACCEPTED);
    }

}
