package com.generators.schema.data.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;
import lombok.NoArgsConstructor;

@Configuration
@ConfigurationProperties(prefix = "project.messages")
@Data
@NoArgsConstructor
public class MessagesConfiguration {

    private String serviceUnavailableCode;
    private String serviceUnavailableReason;
    private String serverErrorCode;
    private String serverErrorReason;
    private String notImplementedCode;
    private String xsdNotImplementedReason;
    private String yamlNotImplementedReason;
    private String xmlToJsonReason;
    private String badRequestCode;
}
