package com.generators.schema.data.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;
import lombok.NoArgsConstructor;

@Configuration
@ConfigurationProperties(prefix = "data.requirements")
@Data
@NoArgsConstructor
public class RequirementsConfiguration {

    private int minimumElementsGenerated;
    private int maximumElementsGenerated;
    private boolean requiredOnly;

}
