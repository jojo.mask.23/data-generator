package com.generators.schema.data.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;
import lombok.NoArgsConstructor;

@Configuration
@ConfigurationProperties(prefix = "data.conversion")
@Data
@NoArgsConstructor
public class ConversionConfiguration {

    private String packageName;
    private String elementName;
    private String tempDir;

}
