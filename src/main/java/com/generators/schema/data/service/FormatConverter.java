package com.generators.schema.data.service;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;

import com.ethlo.jsons2xsd.Config;
import com.ethlo.jsons2xsd.Jsons2Xsd;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class FormatConverter {

    // Not Redundant
    ObjectMapper yamlReader = new ObjectMapper(new YAMLFactory());
    ObjectMapper jsonWriter = new ObjectMapper();

    public String convertYamlToJson(String yaml) throws JsonProcessingException {
        Object obj = yamlReader.readValue(yaml, Object.class);
        return jsonWriter.writeValueAsString(obj);
    }

    /**
     * Method that Converts Json To XSD
     * 
     * @param json
     *            Json Body String To Convert
     * 
     * @return XSD String
     * 
     * @throws IOException
     * @throws TransformerException
     */
    public String convertJsonToXsd(String json, String name) throws IOException, TransformerException {
        final Config cfg = new Config.Builder().targetNamespace("http://example.com/myschema.xsd")
        		.rootElement(name)
        		.name(name)
                .ignoreUnknownFormats(true).build();
        Reader jsonReader = new StringReader(json);
        final Document doc = Jsons2Xsd.convert(jsonReader, cfg);
        jsonReader.close();
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        StringWriter sw = new StringWriter();
        StreamResult sr = new StreamResult(sw);
        DOMSource domSource = new DOMSource(doc);
        transformer.transform(domSource, sr);
        String constructed = sw.toString();
        return constructed;
    }

    public String convertXMLToJson(String xml) throws JSONException {
        JSONObject xmlJSONObj = XML.toJSONObject(xml);
        String json = xmlJSONObj.toString(4);
        if(json.contains("\"xmlns:mys\": \"http://example.com/myschema.xsd\",")) {
        	json = json.replace("\"xmlns:mys\": \"http://example.com/myschema.xsd\",", "");
        	json = json.replace("mys:", "");
        	json = json.replaceAll("(?m)^[ \t]*\r?\n", "");
        }
        return json;
    }
}
