package com.generators.schema.data.service;

import java.rmi.UnexpectedException;
import java.util.ArrayList;
import java.util.List;

import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.SchemaTypeSystem;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.impl.xsd2inst.SampleXmlUtil;
import org.apache.xmlbeans.impl.xsd2inst.SchemaInstanceGenerator;
import org.springframework.stereotype.Service;

import com.generators.schema.data.configuration.RequirementsConfiguration;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@AllArgsConstructor
@Slf4j
public class DataGenerator {

    RequirementsConfiguration requirements;

    public String generateXmlFromDocument(String xsAsString, String elementToGenerate) throws UnexpectedException {
        List<XmlObject> schemaXmlObjects = new ArrayList<>();
        try {
            schemaXmlObjects.add(XmlObject.Factory.parse(xsAsString));
        } catch (XmlException e) {
            log.error("Error Occurred while Parsing Schema", e);
        }
        XmlObject[] xmlObjects = schemaXmlObjects.toArray(new XmlObject[1]);
        SchemaInstanceGenerator.Xsd2InstOptions options = new SchemaInstanceGenerator.Xsd2InstOptions();
        options.setNetworkDownloads(false);
        options.setNopvr(false);
        options.setNoupa(false);
        return xsd2inst(xmlObjects, elementToGenerate, options);
    }

    private String xsd2inst(XmlObject[] schemas, String rootName, SchemaInstanceGenerator.Xsd2InstOptions options)
            throws UnexpectedException {
        SchemaTypeSystem schemaTypeSystem = null;
        if (schemas.length > 0) {
            XmlOptions compileOptions = new XmlOptions();
            if (options.isNetworkDownloads())
                compileOptions.setCompileDownloadUrls();
            if (options.isNopvr())
                compileOptions.setCompileNoPvrRule();
            if (options.isNoupa())
                compileOptions.setCompileNoUpaRule();
            try {
                schemaTypeSystem = XmlBeans.compileXsd(schemas, XmlBeans.getBuiltinTypeSystem(), compileOptions);
            } catch (XmlException e) {
                log.error("Error occurred while compiling XSD", e);
            }
        }
        if (schemaTypeSystem == null) {
            throw new RuntimeException("No Schemas to process.");
        }
        SchemaType[] globalElements = schemaTypeSystem.documentTypes();
        SchemaType elem = null;
        for (SchemaType globalElement : globalElements) {
            if (rootName.equals(globalElement.getDocumentElementName().getLocalPart())) {
                elem = globalElement;
                break;
            }
        }
        if (elem == null) {
            throw new UnexpectedException("Could not find a global element with name \"" + rootName + "\"");
        }
        // Now generate it and return the result
        String result = SampleXmlUtil.createSampleForType(elem);
        log.debug("Produced XML: " + result);
        return result;
    }
}
