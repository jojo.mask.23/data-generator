package com.generators.schema.data.service;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.stereotype.Service;
import org.xml.sax.InputSource;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.generators.schema.data.configuration.RequirementsConfiguration;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@AllArgsConstructor
public class Linter {

    RequirementsConfiguration config;
    ObjectMapper yamlmMapper = new ObjectMapper(new YAMLFactory());
    ObjectMapper jsonMapper = new ObjectMapper();

    public boolean isValidJson(String body) {
        try {
            jsonMapper.readTree(body);
            log.info("Body is valid JSON.");
            return true;
        } catch (IOException e) {
            log.info("Body is not valid JSON.");
            return false;
        }
    }

    public boolean isValidXml(String body) {
        try {
            DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(body)));
            log.info("Body is valid XML.");
            return true;
        } catch (Exception e) {
            log.info("Body is not valid XML.");
            return false;
        }
    }

    public boolean isValidYaml(String body) {
        try {
            // Suppressing as an exception is fired if we can't lint
            @SuppressWarnings("unused")
            Object object = yamlmMapper.readValue(body, Object.class);
            log.info("Body is valid Yaml");
            return true;
        } catch (IOException e) {
            log.info("Body is not valid YAML");
            return false;
        }

    }

    // TODO Assess Usage
    public List<String> processFieldArray(Field[] fields) {
        List<String> result = new ArrayList<>();
        for (Field field : fields) {
            result.add(processField(field));
        }
        return result;

    }

    /**
     * Method to lint the simpleName of a field's type. Usage is to ensure we don't deal with "Custom Classes"
     * 
     * @param field
     *            Field to process
     * 
     * @return String of simple name
     */
    public String processField(Field field) {
        String simpleName = field.getType().getSimpleName();
        String successMessage = simpleName.concat(" detected for field type");
        try {
            JsonProperty requiredValue = field.getAnnotation(JsonProperty.class);
            if (requiredValue.required()) {
                switch (simpleName) {
                case "String":
                case "int":
                case "double":
                case "List":
                    log.info(successMessage);
                    return simpleName;
                default:
                    throw new IllegalArgumentException("Non-Generic Class Detected while processing field: " + field);
                }
            } else {
                return null;
            }
        } catch (NullPointerException e) {
            throw new IllegalArgumentException("Dealing with non-JsonProperty field: " + field);
        }
    }

}
