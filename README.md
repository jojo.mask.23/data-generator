# Schema Data Generator
The Following Project is a Spring Boot-Based Microservice, allowing the user to generate from a JSON Schema

- [Schema Data Generator](#schema-data-generator)
- [Running the Application](#running-the-application)
    * [Management Port Number](#management-port-number)
- [IDE Notes](#ide-notes)
    * [Notes about Eclipse](#notes-about-eclipse)
    * [Notes about Intellij](#notes-about-intellij)
- [Mappings](#mappings)
    * [Generator](#generator)
        + [Json Schema](#json-schema)
            - [Path](#path)
            - [Usage](#usage)
            - [Example Responses](#example-responses)
- [Goals](#goals)
    * [Minimum Data Generation](#minimum-data-generation)
    * [Maximum Data Generation](#maximum-data-generation)
    * [Invalid Data Generation](#invalid-data-generation)
    * [Multi-Array Generation](#multi-array-generation)
- [Logging Information](#logging-information)
    * [Spring Boot Logging](#spring-boot-logging)
    * [Package-Wide Logging](#package-wide-logging)

# Running the Application
Please run this application as a Spring Boot Application on your IDE of choice, or through the command line as per standard

The following host & port is used for this server by default: **localhost:8888**
## Management Port Number
If, for some reason, you wish to access to the management system, the port number is **9999**

# IDE Notes
## Notes about Eclipse
If using Eclipse to run this project, you'll need to install Lombok onto your IDE to get it running, otherwise you ***should*** be fine.  
Details of how to do this can be found here: [Lombok IDE Installation](https://projectlombok.org/setup/eclipse)

## Notes about Intellij
If using IntelliJ to run this project, you'll need to install Lombok onto your IDE to get it running  
Details of how to do this can be found here: [Lombok IDE Installation](https://projectlombok.org/setup/intellij)

# Mappings
## Generator
### Json Schema
Details of the Json Schema Mapping
#### Path
```  
 localhost:8888/generator/jsonSchema 
```

#### Usage
TBC

#### Example Responses
TBC

# Goals
## Minimum Data Generation
TBC

## Maximum Data Generation
TBC

## Invalid Data Generation
TBC

## Multi-Array Generation
TBC

# Logging Information
All Logging Information Is configured in Application.yml
## Spring Boot Logging
The Default Logging Level for Generic Spring Boot Logging is: **ERROR**
## Package-Wide Logging
The Default Logging Level for the Overarching Package is: **DEBUG**